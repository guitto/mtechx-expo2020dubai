<head>
  <meta charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0">

  <title>M-TechX × EXPO 2021 DUBAI UAE</title>
  <meta name="description" content="エムテックス in 2021ドバイ国際博覧会。確かな技術×奇跡の技術を用いて、社会やお客さまに奇跡の風を起こせる製品を紹介します。">
  <meta name="keywords" content="in EXPO 2021 DUBAI,DUBAI,M-TechX">

  <link rel="shortcut icon" href="./assets/images/favicon.ico">
  <meta property="og:description" content="" />
  <meta property="og:image" content="" /><!-- ogp画像は絶対パスで記述する-->
  <link rel="stylesheet" href="./assets/css/style.css">
  <link rel="stylesheet" href="./assets/css/shame.css"><!-- 緊急対応用　常用しない -->
</head>

<body class="<?php echo $bodyClass; ?>">
