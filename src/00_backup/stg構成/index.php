<!DOCTYPE html>
<?php require_once( dirname( __FILE__ ) . '/cms/wp-load.php' ); ?>
<html lang="ja">

<?php include './head.php'; ?>
<?php include './header.php'; ?>

<main>
  <article class="l-wrapper mv">
    <div class="l-inner">
      <div class="mv_title" data-aos="fade-up">
        <h1>
					M-TechX<br>
					×<br>
					EXPO 2021 DUBAI UAE
        </h1>
      </div>
    </div>
    <div class="scrolldown1"><span>Scroll</span></div>
  </article>

  <article class="l-wrapper concept">
    <div class="l-inner">
      <section>
        <div class="concept_txtbox">
          <p>
<?php the_field('concept-text',13); ?>

          </p>
          <h2 data-aos="fade-up">RE VOLUT IONIZ E IN D USTR IES BY<br>
            WORLD-CLASS NANOTECHNOLOGY</h2>
        </div>
      </section>
    </div>
  </article>

  <article class="l-wrapper map" id="map">
    <div class="l-inner">
      <section>
        <h2>MAP</h2>
        <div class="map_imgbox" data-aos="fade-up">
          <picture>
            <source media="(min-width: 640px)" srcset="./assets/images/map_pc.png">
            <img src="./assets/images/map_sp.png" alt="map">
          </picture>
        </div>
        <div class="map_txtbox">
					<?php if(have_rows('booth_loop',110)): ?>
          <?php $i = 0; ?>
					<?php while(have_rows('booth_loop',110)): the_row();  $i++; ?>
		  			<button class="openModal" id="mabtxt<?=$i;?>"><span class="boothNumber">No.<?=$i;?> </span><span class="boothName"><?php the_sub_field('name'); ?></span></button>
					<?php endwhile; ?>
          <?php reset_rows();?>
					<?php endif; ?>

        </div>

				<?php if(have_rows('booth_loop',110)): ?>
        <?php $i = 0; ?>
				<?php while(have_rows('booth_loop',110)): the_row();  $i++; ?>

        <div class="modalArea modalAreaBooth" id="mab<?=$i;?>">
          <div class="modalBg"></div>
          <div class="modalWrapper">
            <div class="modalContents">
              <?php if(get_sub_field('image')): ?>
              <div class="modal-mainImg"><img src="<?php the_sub_field('image'); ?>"></div>
              <?php endif; ?>

              <div class="modal-title">
                <h3><span class="modal-map"><span>No.<?=$i;?>_<?php the_sub_field('name'); ?></h3>
              </div>

              <p class="modal-txt"><?= nl2br(get_sub_field('explain')); ?></p>
              <h4>展示アイテム</h4>
              <ul>
 				        <?php if(have_rows('item')): ?>
                  <?php while(have_rows('item')): the_row(); ?>
<?php
  $link_type = get_sub_field('link_type');
  if($link_type == "manual"):
    if(have_rows('manual')){
			while(have_rows('manual')){
        the_row();
        $item_name = get_sub_field('item_name');
        $item_link = get_sub_field('item_link');
      }
    }
?>
                    <li><a href="<?=$item_link?>" target="_blank" rel="noopener noreferrer">・<?= $item_name; ?></a></li>
<?php
  elseif($link_type == "select"):
    $item_id = get_sub_field('select');
    $item_name = get_field('name', $item_id[0]);
?>
                    <li><a id="matxt<?=$item_id[0];?>" class="selectedItem">・<?= $item_name; ?></a></li>
<?php endif;?>
                   <?php endwhile; ?>
                 <?php endif; ?>
              </ul>


            </div>
            <div class="closeModal">×</div>
          </div>
        </div>

				<?php endwhile; ?>
				<?php endif; ?>

      </section>
    </div>
  </article>

  <article class="modal product" id="product">
    <div class="modal-inner">
      <h2>PRODUCT</h2>
      <!-- openボタン -->
      <div class="product-inner">
<!--メイン-->
				<?php $args = array(
			    'numberposts' => -1, //表示する記事の数
			    'post_type' => 'product' //投稿タイプ名
			    // 条件を追加する場合はここに追記
			  );
			  $customPosts = get_posts($args);
			  if($customPosts) : foreach($customPosts as $post) : setup_postdata( $post );
			  ?>

					<button class="openModal" id="om<?= $post->ID; ?>">
					  <div class="product-img"><span class="product-img-arrow"></span><img src="<?php the_field('image'); ?>"></div>
					  <div class="product-map"><?php the_field('booth-num'); ?></div>
					  <h3><?php the_field('name'); ?></h3>
					  <p class="product-txt"><?= nl2br(get_sub_field('caption')); ?></p>
					  <div class="product-tag">
							<?php if(have_rows('tag')): ?>
							<?php while(have_rows('tag')): the_row(); ?>
					  		<span><?php the_sub_field('tag-text'); ?></span>
							<?php endwhile; ?>
							<?php endif; ?>
					  </div>
					</button>

			  <?php endforeach; ?>
			  <?php else : //記事が無い場合 ?>
			  <p>Sorry, no posts matched your criteria.</p>
			  <?php endif;
			  wp_reset_postdata(); //クエリのリセット
				$args = array(
					'numberposts' => -1, //表示する記事の数
					'post_type' => 'product' //投稿タイプ名
					// 条件を追加する場合はここに追記
				);
				$customPosts = get_posts($args);
?>

<!--モーダル-->
			<?php if($customPosts) : foreach($customPosts as $post) : setup_postdata( $post ); ?>

				<!-- モーダル部分 -->
        <div class="modalArea" id="ma<?= $post->ID; ?>">
          <div class="modalBg"></div>
          <div class="modalWrapper">
            <div class="modalContents">
              <div class="modal-title">
                <h3><span class="modal-map"><?php the_field('booth-num'); ?></span><?php the_field('name'); ?></h3>
                <p class="modal-txt"><?php the_field('caption'); ?></p>
                <div class="product-tag">
                  <?php if(have_rows('tag')): ?>
                  <?php while(have_rows('tag')): the_row(); ?>
                    <span><?php the_sub_field('tag-text'); ?></span>
                  <?php endwhile; ?>
                  <?php endif; ?>
                </div>
              </div>
              <div class="modal-mainImg"><img src="<?php the_field('image'); ?>"></div>
              <div class="modal-subImg"><img src="<?php the_field('modalimg1'); ?>"></div>
              <div class="modal-subImg"><img src="<?php the_field('modalimg2'); ?>"></div>
              <div class="modal-subImg"><img src="<?php the_field('modalimg3'); ?>"></div>
              <table>
                <tr>
                  <th>製品名／ブランド名</th>
                  <td>
                    <h4><?php the_field('company-name'); ?></h4>
                  </td>
                </tr>
                <tr>
                  <th>展示内容説明</th>
                  <td>
										<?php if(have_rows('detail')): ?>
										<?php while(have_rows('detail')): the_row(); ?>
											<h4><?php the_sub_field('index'); ?></h4>
	                    <p><?= nl2br(get_sub_field('text'));?></p>

										<?php endwhile; ?>
										<?php endif; ?>

                  </td>
                </tr>
                <tr>
                  <th>見どころ</th>
                  <td>
                    <p><?= nl2br(the_field('point'));?></p>
                  </td>
                </tr>

              </table>

              <h5>本製品のお問い合わせは<a href="https://www.mtechx.co.jp/" target="_blank" rel="noopener noreferrer">M-TEchX Inc.</a>までよろしくお願いします。</h5>
            </div>
            <div class="closeModal">×</div>
          </div>
        </div>

			<?php endforeach; ?>
			<?php else : //記事が無い場合 ?>
			<p>Sorry, no posts matched your criteria.</p>
			<?php endif;
			wp_reset_postdata(); //クエリのリセット ?>


      </div>
  </article>



  </div>
  </section>
  </div>
  </article>

  <article class="l-wrapper contact" id="contact">
    <div class="l-inner">
      <section>
        <h2>CONTACT</h2>
				<?php //echo do_shortcode( '[contact-form-7 id="92" title="コンタクトフォーム 1"]' ); ?>

        <div class="box_con">
          <form method="post" action="mail.php">
            <p>ご質問・ご相談はお気軽にお問い合わせください。</p>
            <table class="formTable">
              <tr>
                <th>御社名<span>必須</span></th>
                <td><input type="text" name="御社名" required placeholder="(例)〇〇株式会社"></td>
              </tr>
              <tr>
                <th>部署名</th>
                <td><input type="text" name="部署名" placeholder="(例)営業部"></td>
              </tr>
              <tr>
                <th>ご担当者のお名前<span>必須</span></th>
                <td><input type="text" name="ご担当者のお名前" required placeholder="ご担当者のお名前"></td>
              </tr>
              <tr>
                <th>所在地</th>
                <td><input type="text" name="所在地" placeholder="所在地"></td>
              </tr>
              <tr>
                <th>電話番号（半角）<span>必須</span></th>
                <td><input type="text" name="電話番号" required placeholder="(例)00-0000-0000"></td>
              </tr>
              <tr>
                <th>E-mail（半角）</th>
                <td><input type="text" name="E-mail" placeholder="(例)xxx@xxx.com"></td>
              </tr>
              <tr>
                <th>お問い合わせ項目</th>
                <td><select name="ご用件">
                    <option value="">選択してください</option>
                    <option value="商品取扱について">商品取扱について</option>
                    <option value="商品開発について">商品開発について</option>
                    <option value="取材申込について">取材申込について</option>
                    <option value="資料請求・サンプル希望">資料請求・サンプル希望</option>
                    <option value="採用について">採用について</option>
                    <option value="その他">その他</option>
                  </select></td>
              </tr>
              <tr>
                <th>お問い合わせ内容<span>必須</span></th>
                <td><textarea name="お問い合わせ内容" cols="50" rows="5" required placeholder="お問い合わせ内容"></textarea></td>
              </tr>
            </table>
            <div class="box_check">
              <label>
                <input type="checkbox" name="acceptance-714" value="1" aria-invalid="false" class="agree"><span class="check">プライバシーポリシーに同意する</span>
              </label>
            </div>
            <p class="btn">
              <span><input type="submit" value="　 CONTACT 　" /></span>
            </p>
          </form>
        </div>
      </section>
    </div>
  </article>
  <div class="official-site_btn">
    <a href="http://www.mtechx.co.jp/eng/" target="_blank" rel="noopener noreferrer">Click here for the official site<img src="assets/images/link.svg"></a>
  </div>
  <article class="l-wrapper outline">
    <div class="l-inner">
      <section>
        <h2>OUTLINE</h2>
        <table>
					<?php if(have_rows('outline',13)): ?>
					<?php while(have_rows('outline',13)): the_row(); ?>

					<tr>
            <th><?php the_sub_field('th'); ?></th>
            <td><?= nl2br(get_sub_field('td')); ?></td>
          </tr>
					<?php endwhile; ?>
					<?php endif; ?>
        </table>
      </section>
    </div>
  </article>
</main>

<?php include './footer.php'; ?>

</html>
