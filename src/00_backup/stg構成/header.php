<header class="l-header">
  <div class="navBox">
    <div class="nav_left">
      <a href="#">
        <img src="assets/images/MtechX_logo_S_color_r_inc_w.svg" alt="企業ロゴ">
      </a>
    </div>
    <div class="nav_right">
      <div class="nav_menuBox u-pc">
        <a href="#map" class="nav_menu">
          <p>MAP</p>
        </a>
        <a href="#product" class="nav_menu">
          <p>PRODUCT</p>
        </a>
      </div>
      <a href="#contact" class="nav_cv-btn">CONTACT</a>
      <div class="burger-btn u-sp">
        <!-- ③ハンバーガーボタン -->
        <span class="bar bar_top"></span>
        <span class="bar bar_mid"></span>
        <span class="bar bar_bottom"></span>
      </div>
      <div class="nav-wrapper u-sp">
        <!-- ②ナビゲーションメニュー -->
        <nav class="header-nav" id="header-nav">
          <ul class="nav-list">
            <li class="nav-item"><a href="#map">MAP</a></li>
            <li class="nav-item"><a href="#product">PRODUCT</a></li>
            <li class="nav-item"><a href="#contact">CONTACT</a></li>
          </ul>
        </nav>
      </div>
    </div>
  </div>
</header>

<p id="page-top"><a href="#">Page Top</a></p>
