<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link https://ja.wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// 注意:
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.osdn.jp/%E7%94%A8%E8%AA%9E%E9%9B%86#.E3.83.86.E3.82.AD.E3.82.B9.E3.83.88.E3.82.A8.E3.83.87.E3.82.A3.E3.82.BF 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define( 'DB_NAME', 'guitto_mtechx' );

/** MySQL データベースのユーザー名 */
define( 'DB_USER', 'guitto_mtechx' );

/** MySQL データベースのパスワード */
define( 'DB_PASSWORD', '20211015' );

/** MySQL のホスト名 */
define( 'DB_HOST', 'mysql705b.xserver.jp' );

/** データベースのテーブルを作成する際のデータベースの文字セット */
define( 'DB_CHARSET', 'utf8mb4' );

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define( 'DB_COLLATE', '' );

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'jP`FOpwRM1B4axQ%zO%LK(Pg$.k.KfAT3=up{Gh6;~SF{<qD%0|H$I$hDpKmIk?]' );
define( 'SECURE_AUTH_KEY',  'K,9.&Ko|EoW&b`&!!2}yR2~|cj ~;9nhc=Xj^:<phe{a}Qa;s$y1;o|Y*ZOo:^Yf' );
define( 'LOGGED_IN_KEY',    '?)>.8n,47q6|QK.m%=.wGm$mNcd%76~s@}T57!|:C5#n$+%PITI$Lz`z+Fc}=L^]' );
define( 'NONCE_KEY',        '$53mI,jCR-r!`8q|u:gpwq(&uJj/~+Qd 0K/H}zm@ndm-t!=K8~c<c qu}tE+o$r' );
define( 'AUTH_SALT',        '4pj}DcA`>@zVLwL071/<&^+n2*yMa^I1p=$<qsK605c@JKo{=ewap?R={yWI~]dK' );
define( 'SECURE_AUTH_SALT', '%3D^I-UIF8G]R%Ej=_LM8{0-03# J>&[xBEJp Z///r(#30=-]Xw(p.j8.T!K*Oz' );
define( 'LOGGED_IN_SALT',   'hZG;Zi}8_at`,9oaVBQ(31/GEAVbRbGg9B{(sTh:(FWJLd<FN5x`pf4,b^GGvKl0' );
define( 'NONCE_SALT',       'Yw~%e?;#(pm$4vnr}5P,F${NkVpJz#kR>-aPPD}_|Lv,JH|+zjQNvsm-NNY+MHA7' );

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix = 'wpdubai_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数についてはドキュメンテーションをご覧ください。
 *
 * @link https://ja.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* カスタム値は、この行と「編集が必要なのはここまでです」の行の間に追加してください。 */



/* 編集が必要なのはここまでです ! WordPress でのパブリッシングをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
