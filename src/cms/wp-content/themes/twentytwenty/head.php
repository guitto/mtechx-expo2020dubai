<head>
  <meta charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0">

    <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-K8JL7R3');</script>
  <!-- End Google Tag Manager -->

  <title>PLANET JAPAN by M-TEchX at DEC North HALL 1, Dubai Expo 2020</title>
  <meta name="description" content="Experience the best of Japan’s world-famous cutting-edge technologies, products, services and culture.">

  <link rel="shortcut icon" href="<?=$theme_uri;?>/assets/images/favicon.ico">
  <meta property="og:description" content="" />
  <meta property="og:image" content="" /><!-- ogp画像は絶対パスで記述する-->

  <link rel="stylesheet" href="<?=$theme_uri;?>/assets/css/style.css">
  <link rel="stylesheet" href="<?=$theme_uri;?>/assets/css/shame.css"><!-- 緊急対応用　常用しない -->
</head>

<body class="<?php echo $bodyClass; ?>">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K8JL7R3"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
