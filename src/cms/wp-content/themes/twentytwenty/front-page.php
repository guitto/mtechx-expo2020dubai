<!DOCTYPE html>

<html lang="ja">
<?php $dir = get_template_directory(); ?>
<?php $theme_uri = get_template_directory_uri(); ?>
<?php include $dir.'/head.php'; ?>
<?php include $dir.'/header.php'; ?>

<main>
  <article class="l-wrapper mv">
    <div class="l-inner">
      <div class="mv_title" data-aos="fade-up">
        <h1>
          <img src="<?=$theme_uri;?>/assets/images/pj-logo.png" alt="PLANET JAPAN by M-TEchX">
        </h1>
        <h2>
          at DEC North HALL 1, Dubai Expo 2020, UAE
        </h2>
        <h3>December 2 thu. ～ 4 sat.2021 Dubai Exhibition Centre（DEC)<br>Open 10:00 ～ 22:00</h3>
      </div>
      <a href="https://dubai.platinumlist.net/event-tickets/82590/planet-japan-by-m-techx" target="_blank" rel="norefferer noopener" class="register-btn">REGISTER</a>
    </div>

  </article>

  <article class="l-wrapper concept">
    <div class="l-inner">
      <section>
        <div class="concept_txtbox">
          <p>
<?php the_field('concept-text',13); ?>

          </p>
          <h2 data-aos="fade-up">Experience the best of Japan’s world-famous cutting-edge technologies, products, services and culture.</h2>
        </div>
      </section>
    </div>
  </article>

  <article class="l-wrapper outline">
    <div class="l-inner">
      <section>
        <h2>OUTLINE</h2>
        <table>
					<?php if(have_rows('outline',13)): ?>
					<?php while(have_rows('outline',13)): the_row(); ?>

					<tr>
            <th><?php the_sub_field('th'); ?></th>
            <td><?= nl2br(get_sub_field('td')); ?></td>
          </tr>
					<?php endwhile; ?>
					<?php endif; ?>
        </table>
      </section>
    </div>
  </article>

  <article class="l-wrapper map" id="map">
    <div class="l-inner">
      <section>
        <h2>MAP</h2>
        <div class="map_imgbox" data-aos="fade-up">
          <picture>
            <source media="(min-width: 640px)" srcset="<?=$theme_uri;?>/assets/images/map_pc.png?20211225">
            <img src="<?=$theme_uri;?>/assets/images/map_sp.png?20211225" alt="map">
          </picture>
        </div>
        <div class="map_txtbox">
					<?php if(have_rows('booth_loop',110)): ?>
          <?php $i = 0; ?>
					<?php while(have_rows('booth_loop',110)): the_row();  $i++; ?>
		  			<button class="openModal" id="mabtxt<?=$i;?>"><span class="boothNumber">No.<?=$i;?> </span><span class="boothName"><?php the_sub_field('name'); ?></span></button>
					<?php endwhile; ?>
          <?php reset_rows();?>
					<?php endif; ?>

        </div>

				<?php if(have_rows('booth_loop',110)): ?>
        <?php $i = 0; ?>
				<?php while(have_rows('booth_loop',110)): the_row();  $i++; ?>

        <div class="modalArea modalAreaBooth" id="mab<?=$i;?>">
          <div class="modalBg"></div>
          <div class="modalWrapper">
            <div class="modalContents">
              <?php if(get_sub_field('image')): ?>
              <div class="modal-mainImg"><img src="<?php the_sub_field('image'); ?>"></div>
              <?php endif; ?>

              <div class="modal-title">
                <h3><span class="modal-map"><span>No.<?=$i;?>_<?php the_sub_field('name'); ?></h3>
              </div>

              <p class="modal-txt"><?= nl2br(get_sub_field('explain')); ?></p>
              <h4>Display Items</h4>
              <ul>
 				        <?php if(have_rows('item')): ?>
                  <?php while(have_rows('item')): the_row(); ?>
<?php
  $link_type = get_sub_field('link_type');
  if($link_type == "manual"):
    if(have_rows('manual')){
			while(have_rows('manual')){
        the_row();
        $item_name = get_sub_field('item_name');
        $item_link = get_sub_field('item_link');
      }
    }
?>
                    <li><a href="<?=$item_link?>" target="_blank" rel="noopener noreferrer">・<?= $item_name; ?></a></li>
<?php
  elseif($link_type == "select"):
    $item_id = get_sub_field('select');
    $item_name = get_field('name', $item_id[0]);
?>
                    <li><a id="matxt<?=$item_id[0];?>" class="selectedItem">・<?= $item_name; ?></a></li>
<?php endif;?>
                   <?php endwhile; ?>
                 <?php endif; ?>
              </ul>


            </div>
            <div class="closeModal">×</div>
          </div>
        </div>

				<?php endwhile; ?>
				<?php endif; ?>

      </section>
    </div>
  </article>

  <article class="modal product" id="product">
    <div class="modal-inner">
      <h2>PRODUCT</h2>
      <!-- openボタン -->
      <div class="product-inner">
<!--メイン-->
				<?php $args = array(
			    'numberposts' => -1, //表示する記事の数
			    'post_type' => 'product' //投稿タイプ名
			    // 条件を追加する場合はここに追記
			  );
			  $customPosts = get_posts($args);
			  if($customPosts) : foreach($customPosts as $post) : setup_postdata( $post );
			  ?>

					<button class="openModal" id="om<?= $post->ID; ?>">
					  <div class="product-img"><span class="product-img-arrow"></span><img src="<?php the_field('image'); ?>"></div>
					  <div class="product-map"><?php the_field('booth-num'); ?></div>
					  <h3><?php the_field('name'); ?></h3>
					  <p class="product-txt"><?= nl2br(get_sub_field('caption')); ?></p>
					  <div class="product-tag">
							<?php if(have_rows('tag')): ?>
							<?php while(have_rows('tag')): the_row(); ?>
					  		<span><?php the_sub_field('tag-text'); ?></span>
							<?php endwhile; ?>
							<?php endif; ?>
					  </div>
					</button>

			  <?php endforeach; ?>
			  <?php else : //記事が無い場合 ?>
			  <p>Sorry, no posts matched your criteria.</p>
			  <?php endif;
			  wp_reset_postdata(); //クエリのリセット
				$args = array(
					'numberposts' => -1, //表示する記事の数
					'post_type' => 'product' //投稿タイプ名
					// 条件を追加する場合はここに追記
				);
				$customPosts = get_posts($args);
?>

<!--モーダル-->
			<?php if($customPosts) : foreach($customPosts as $post) : setup_postdata( $post ); ?>

				<!-- モーダル部分 -->
        <div class="modalArea" id="ma<?= $post->ID; ?>">
          <div class="modalBg"></div>
          <div class="modalWrapper">
            <div class="modalContents">
              <div class="modal-title">
                <h3><span class="modal-map"><?php the_field('booth-num'); ?></span><?php the_field('name'); ?></h3>
                <p class="modal-txt"><?php the_field('caption'); ?></p>
                <div class="product-tag">
                  <?php if(have_rows('tag')): ?>
                  <?php while(have_rows('tag')): the_row(); ?>
                    <span><?php the_sub_field('tag-text'); ?></span>
                  <?php endwhile; ?>
                  <?php endif; ?>
                </div>
              </div>
              <div class="modal-mainImg"><img src="<?php the_field('image'); ?>"></div>

              <?php if(get_field('modalimg1')):?><div class="modal-subImg"><img src="<?php the_field('modalimg1'); ?>"></div><?php endif; ?>
              <?php if(get_field('modalimg2')):?><div class="modal-subImg"><img src="<?php the_field('modalimg2'); ?>"></div><?php endif; ?>
              <?php if(get_field('modalimg3')):?><div class="modal-subImg"><img src="<?php the_field('modalimg3'); ?>"></div><?php endif; ?>
              <table>
                <tr>
                  <th>Name or brand of product</th>
                  <td>
                    <h4><?php the_field('company-name'); ?></h4>
                  </td>
                </tr>
                <tr>
                  <th>Product Description</th>
                  <td>
										<?php if(have_rows('detail')): ?>
										<?php while(have_rows('detail')): the_row(); ?>
											<h4><?php the_sub_field('index'); ?></h4>
	                    <p><?= nl2br(get_sub_field('text'));?></p>

										<?php endwhile; ?>
										<?php endif; ?>

                  </td>
                </tr>
              </table>

              <h5><a href="#contact" target="_blank" rel="noopener noreferrer">Click here to contact us.</a></h5>
            </div>
            <div class="closeModal">×</div>
          </div>
        </div>

			<?php endforeach; ?>
			<?php else : //記事が無い場合 ?>
			<p>Sorry, no posts matched your criteria.</p>
			<?php endif;
			wp_reset_postdata(); //クエリのリセット ?>


      </div>
  </article>



  </div>
  </section>
  </div>
  </article>

  <article class="l-wrapper contact" id="contact">
    <div class="l-inner">
      <section>
        <h2>CONTACT</h2>
        <div class="box_con">
				  <?php echo do_shortcode( '[contact-form-7 id="358" title="コンタクトフォーム 1"]' ); ?>
        </div>

      </section>
    </div>
  </article>
  <div class="official-site_btn">
    <a href="http://www.mtechx.co.jp/eng/" target="_blank" rel="noopener noreferrer">official site<img src="<?=$theme_uri;?>/assets/images/link.svg"></a>
  </div>

  <article class="l-wrapper exhibition">
    <div class="l-inner">
      <section>
        <h2>EXHIBITION SUPPORT</h2>
        <table>
					<?php if(have_rows('exhibition',13)): ?>
					<?php while(have_rows('exhibition',13)): the_row(); ?>

					<tr>
            <th><?php the_sub_field('th'); ?></th>
            <td><?= nl2br(get_sub_field('td')); ?></td>
          </tr>
					<?php endwhile; ?>
					<?php endif; ?>
        </table>
      </section>

      <a href="https://dubai.platinumlist.net/event-tickets/82590/planet-japan-by-m-techx" target="_blank" rel="norefferer noopener" class="register-btn">REGISTER</a>
    </div>
  </article>
</main>

<?php include $dir.'/footer.php'; ?>
<?php include $dir.'/footer-js.php'; ?>
</html>
