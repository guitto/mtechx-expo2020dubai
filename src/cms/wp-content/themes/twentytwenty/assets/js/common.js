"use strict";
//コンテンツの動き プラグインAOS
AOS.init()
$(function () {
  /*$('#js-target').t({
    speed_vary: true,
    init: function (elm) {
      console.log('init');
    },
    typing: function (elm, left, total) {
      console.log('typing');
    },
    fin: function (elm) {
      console.log('fin');
    }
  });*/
});


//ページ内リンク系
$(function () {
  $('a[href^="#"]').click(function () {
    // 移動先を0px調整する。0を30にすると30px下にずらすことができる。
    var adjust = -50;
    var speed = 400;
    var href = $(this).attr("href");
    var target = $(href == "#" || href == "" ? 'html' : href);
    var position = target.offset().top + adjust;
    $('body,html').animate({ scrollTop: position }, speed, 'swing');
    return false;
  });

});

// #page-topの設定
function PageTopAnime() {
  var scroll = $(window).scrollTop();
  if (scroll >= 200) {
    $('#page-top').removeClass('RightMove');
    $('#page-top').addClass('LeftMove');
  } else {
    if (
      $('#page-top').hasClass('LeftMove')) {
      $('#page-top').removeClass('LeftMove');
      $('#page-top').addClass('RightMove');
    }
  }
}
$(window).scroll(function () {
  PageTopAnime();/* スクロールした際の動きの関数を呼ぶ*/
});
$(window).on('load', function () {
  PageTopAnime();/* スクロールした際の動きの関数を呼ぶ*/
});

//ハンバーガー
$('.burger-btn').on('click', function () {
  $('.burger-btn').toggleClass('close');
  $('.nav-wrapper').toggleClass('slide-in');
  $('body').toggleClass('noscroll');
});
$('#header-nav a[href]').on('click', function (event) {
  $('.burger-btn').toggleClass('close');
  $('.nav-wrapper').toggleClass('slide-in');
});

$(function () {
  /* map modal */
  $('.map .openModal').click(function () {  //openModalをクリックした時に
    var btnId = $(this).attr("id").replace("mabtxt", "");

    $('#mab' + btnId).addClass('is-open');
    //クリックしたモーダルボタンと同じ番目のモーダルを表示する。addClassでis-openクラスを追加して表示する
    $('html, body').css('overflow', 'hidden');
    // overflow:hidden;の追加で背景のスクロール禁止に
  });

  $('.modalArea .selectedItem').click(function () {  //openModalをクリックした時に
    var btnId = $(this).attr("id").replace("matxt", "");

    $('#ma' + btnId).addClass('is-open');
    //クリックしたモーダルボタンと同じ番目のモーダルを表示する。addClassでis-openクラスを追加して表示する
    $('html, body').css('overflow', 'hidden');
    // overflow:hidden;の追加で背景のスクロール禁止に
  });

  /* product modal */
  $('.product .openModal').click(function () {  //openModalをクリックした時に
    var btnId = $(this).attr("id").replace("om", "");

    $('#ma' + btnId).addClass('is-open');
    //クリックしたモーダルボタンと同じ番目のモーダルを表示する。addClassでis-openクラスを追加して表示する
    $('html, body').css('overflow', 'hidden');
    // overflow:hidden;の追加で背景のスクロール禁止に
  });


  $('.closeModal, .modalBg,.product .modalWrapper a').click(function () { //closeModalかmodalBgをクリックした時に
    $('.modalArea').removeClass('is-open');
    //モーダルを非表示にする。removeClassでis-openクラスを削除して非表示にする
    $('html, body').removeAttr('style');
    //追加したoverflow:hidden;を削除
  });

});
