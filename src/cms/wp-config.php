<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link https://ja.wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// 注意:
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.osdn.jp/%E7%94%A8%E8%AA%9E%E9%9B%86#.E3.83.86.E3.82.AD.E3.82.B9.E3.83.88.E3.82.A8.E3.83.87.E3.82.A3.E3.82.BF 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define( 'DB_NAME', 'mtechx2_expo' );

/** MySQL データベースのユーザー名 */
define( 'DB_USER', 'mtechx2' );

/** MySQL データベースのパスワード */
define( 'DB_PASSWORD', '6tddGiu5rF7' );

/** MySQL のホスト名 */
define( 'DB_HOST', 'mysql57.mtechx2.sakura.ne.jp' );

/** データベースのテーブルを作成する際のデータベースの文字セット */
define( 'DB_CHARSET', 'utf8mb4' );

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define( 'DB_COLLATE', '' );

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Ja63*b=klKnynTF?{TS]c%p?Sm:@Mb%e*fKn^AYN @A6rB.4.^y%yf;bZH>..48L' );
define( 'SECURE_AUTH_KEY',  'K]S&kb`2~^H<^w?VP@4xR9_O*:QOIVRHIT-:G9#jHMU>f17U5 @1jh={2.: egnF' );
define( 'LOGGED_IN_KEY',    'u<jS$?MwfC88xy;#O6]T>ha&Y{M,afhr0GJpqE6!qxx+r;I$Ak Le3sSQ0,kOvG-' );
define( 'NONCE_KEY',        'fr@RhH>pZLpt%)`z~}K(GefCN{Tck*%<cj3!lD24d,USITJt+ (4<s^{ndlOg+uT' );
define( 'AUTH_SALT',        'bX=eT)%]8j*e0[K/R7NS98?=)]f(z)@ulPHGf0J+h>EhC`bejCOp/%ENJf%X:*!y' );
define( 'SECURE_AUTH_SALT', 'K_w=/Z$m;35(m=%ax}YlDfEfc;1_:d:z:z80*tJMS&(_O2IAv%QOC(+1/_PW&U&Q' );
define( 'LOGGED_IN_SALT',   '7<3,UB(v2%d`g^-=Ol-D-ifp=P9f%ria lLZT@Npt&E^@$aEa}J&%R6%3PH)Boop' );
define( 'NONCE_SALT',       'tRG?7-:MR;%En$9Nr>t!8Hg|w)}in}.ddrGxo/XvXt9 Ox( Y1lbA:S0A_#7egpr' );

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix = 'wp0d70a7';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数についてはドキュメンテーションをご覧ください。
 *
 * @link https://ja.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* カスタム値は、この行と「編集が必要なのはここまでです」の行の間に追加してください。 */



/* 編集が必要なのはここまでです ! WordPress でのパブリッシングをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
