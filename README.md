# コーディングスターターセット概要

コーディングのベースセットです

header footer等はPHPでインクルードしています

CSS設計はFLOCSSベースです

DartSassでSCSSからCSSにコンパイルします

Postcssのautoprefixerでベンダープレフィックスの自動付与します

swiperをよく使うのでpluginに入れてます（不要なら削除）

footer-js.phpによく使うCDNを入れています（使う場合はコメントアウト外す、不要なら削除）

各種ライブラリやプラグインについては、利用時には最新版ではない可能性があります。
納品までに最新版に差し替えてください。

css/shame.cssは緊急時の上書き用のCSSなので、常用はしません。
（緊急の対応が完了したら適宜SCSSにマージします、通常空になっているのが理想）


## クローン後のセットアップ手順（初回のみ）

VSCodeでプロジェクトを開きます

VSCodeで「ターミナル」→「新しいターミナル」を開きます

下記コマンドでpackage.jsonを作ります
```
npm init -y
```

package-sample.jsonから必要箇所をpackage.jsonにコピペします
カンマの不足があると動かないので、気を付けてください。

```
  "scripts": {
    "sass": "sass --style=compressed src/assets/scss/:src/assets/css/ --watch",
    "postcss": "postcss src/assets/css/style.css -u autoprefixer -o src/assets/css/style.css"
  }
```
```
  "devDependencies": {
    "autoprefixer": "^10.2.5",
    "postcss": "^8.2.14",
    "postcss-cli": "^8.3.1",
    "sass": "^1.32.12"
  }
```
devDependencies内の各パッケージはできれば最新版に変更しましょう

「npm view ＜パッケージ名＞」でリリースされている最新バージョンが確認できます
↓例
```
npm view autoprefixer
```

パッケージをインストールします
```
npm i -D
```

## SCSSのコンパイルの方法

```
npm run sass
```
で、SCSSファイルの監視状態になります。更新すると自動でコンパイルが走ります。


## ベンダープレフィックスの付与

```
npm run postcss
```
でcss/style.css内の記述にベンタープレフィックスを追加します。
style.cssが存在しないと動かないので注意。
